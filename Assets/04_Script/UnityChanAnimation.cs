﻿using UnityEngine;
using System.Collections;

public class UnityChanAnimation : MonoBehaviour {

    Animator AnimatorUnityChan;
    public bool BoolWalk, BoolIdle;
    public float FloatWalkSpeed;

	// Use this for initialization
	void Start () {
        AnimatorUnityChan = GetComponent < Animator >( );
	}
	
	// Update is called once per frame
	void Update () {
        AnimatorUnityChan.SetBool("BoolWalk", BoolWalk);
        AnimatorUnityChan.SetFloat("FloatWalkSpeed", FloatWalkSpeed);
	
	}
}
