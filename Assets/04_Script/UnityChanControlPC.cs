﻿using UnityEngine;
using System.Collections;

public class UnityChanControlPC : MonoBehaviour {

    public float UnityChanWalkSpeed;
    public float positionY;
    
	// Use this for initialization
	void Start () {

        this.transform.position = new Vector3((float)0.0, positionY, (float)0.0);
    }
	
	// Update is called once per frame
	void Update () {
        float moveHorizontal = Input.GetAxis("Horizontal");
        {
            if (moveHorizontal != 0)
            {
                Vector3 movement = new Vector3(moveHorizontal * this.UnityChanWalkSpeed * Mathf.Sign(moveHorizontal) * Time.deltaTime, (float)0.0, (float)0.0);
                this.transform.Translate(movement);

                this.transform.localScale = new Vector3(Mathf.Sign(moveHorizontal),(float)1.0,(float)1.0);
                GetComponent<UnityChanAnimation>().BoolWalk = true;
            }
            else
            {
                GetComponent<UnityChanAnimation>().BoolWalk = false;
            }
        }
	}
}
